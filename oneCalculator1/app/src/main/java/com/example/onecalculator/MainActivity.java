package com.example.onecalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.DrawableCompat;

import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.icu.util.ValueIterator;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView screen;
    private String str1, str2, result, str, sign;
    private double a,b;
    LinearLayout op;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        getSupportActionBar().hide();
        op = (LinearLayout) findViewById(R.id.text_layout);
        screen = (TextView) findViewById(R.id.textView);
        str = "";
    }
    public void onClick(View v){
        Button button = (Button) v;
        str += button.getText().toString();
        screen.setText(str);

    }
    public void onClickSign(View v){
        Button button = (Button) v;
        sign = button.getText().toString();
        try{
            str = screen.getText().toString();
            a = Double.parseDouble(str);
            screen.setText(sign);
            str = "";
        }catch (Exception e){
            Toast.makeText(MainActivity.this, "Angka yang dioperasikan tidak ada !",Toast.LENGTH_LONG).show();
        }
    }
    public void calculate(View v){
        Button button = (Button) v;
        str2 = screen.getText().toString();
//        if (a == 1 && str2.equals("+")){
//            op.setBackgroundResource(R.drawable.neversettle);;
//        }
        try
        {
            b = Double.parseDouble(str2);
            if (sign.equals("+")){
                result = a+b+"";
            }
            else if (sign.equals("-")){
                result = a-b+"";
            }
            else if (sign.equals("*")){
                result = a*b+"";
            }
            else if (sign.equals("/")){
                result = a/b+"";
            }
            else{
                result = "i feel something wrong about this";
            }
            screen.setText(result);
        }
        catch(Exception e)
        {
            Toast.makeText(MainActivity.this, "Bereskan Inputan !",Toast.LENGTH_LONG).show();
        }

    }
    public void onClickDel(View v){
        screen.setText("");
        str = "";
        str1 = "";
        str2 = "";
        sign = "";
        a = 0;
        b = 0;
    }
}
